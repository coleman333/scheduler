var passport = require('passport');
var path = require('path');
var moment = require('moment');
var multer = require('multer');
let bcrypt = require('bcrypt');
let SALT_WORK_FACTOR = 9;
let userModel = require('../models/userModel');

module.exports.upload = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            var absolutePath = path.join(__dirname, '../../images/');
            cb(null, absolutePath);
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        }
    })
});

module.exports.registration = function (req, res, next) {
    // console.dir(JSON.parse(req.body.user));
    req.body.user=JSON.parse(req.body.user);
    // console.dir(req.body);
    // console.dir(req.file);
    let user = req.body.user;
    encryptPassword(user.password, function (err, encryptedPassword) {
        if (err) {
            console.error(err);
            return next(err);
        }
        user.password = encryptedPassword;
        try {
            user.avatar = `/images/${req.file.filename}`; //'images'+req.file.filename
        }catch(ex){}
        userModel.create(user, function (err, user) {
            if (err) {
                console.error(err);
                return next(err);
            }
            // login2(req.body.user);
            res.json({
                name: user.name,
                avatar: user.avatar
            });
        });

    });
};

function encryptPassword(password, callbackEncryptPassword) {
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return callbackEncryptPassword(err);

        // hash the password along with our new salt
        bcrypt.hash(password, salt, function (err, hash) {
            if (err) return callbackEncryptPassword(err);
            // override the cleartext password with the hashed one
            callbackEncryptPassword(null, hash);
        });
    });
}


module.exports.login = function (req, res, next) {
    passport.authenticate('local', function done(err, user, info) {
            // let userModel ={};
            // userModel.find({category: req.params.email},function (err,user) {
            //
            // });
        if (err) {
            console.error(err);
            return next(err);
        }
        if (user) {
            req.logIn(user, function (err) {
                if (err) {
                    console.error(err);
                    return next(err);
                }
                res.json({
                    name: user.name,
                    avatar:user.avatar
                    // message: 'You have access!'
                });
            });
        } else {
            res.statusCode = 403;
            res.json({
                message: info.message
            });
        }
    })(req, res, next);
};

module.exports.isUser = function (req,res,next) {
    if (req.isAuthenticated()) {
        // console.log(req.user);
        res.json({
            name: req.user.name,
            avatar:req.user.avatar
        });
    }
    else {
        res.statusCode = 403;
        res.end()
    }  
};

module.exports.logout = function (req, res, next) {
    req.logOut();
    res.end();
};





