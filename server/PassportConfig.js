var LocalStrategy = require('passport-local').Strategy;
var User = require('./models/userModel');
var bcrypt = require('bcrypt');

module.exports = function (passport) {
    passport.use(new LocalStrategy(
        {usernameField: 'email'},
        function (email, password, done) {
            User.findOne({email: email}, function (err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {message: 'Incorrect email.'});
                }
                bcrypt.compare(password,user.password, function x(err, result) {
                    if (err) {
                        console.error(err);
                        return done(err);
                    }
                    if (!result) {
                        return done(null, false, {message: 'Incorrect password.'});
                    }
                    return done(null, user);
                });
            });
        }
    ));

    passport.serializeUser(function (user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, userObj) {
            done(err, userObj); //req.user._id
        });
    });
}
