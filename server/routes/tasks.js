var express = require('express');
var router = express.Router();
let multer =require('multer');
var tasksCtrl = require('../controllers/tasksCtrl');

router.post('/showCommonTasks',tasksCtrl.showCommonTasks);
router.post('/createTask',tasksCtrl.createTask);
router.post('/updateTask',tasksCtrl.updateTask);
router.post('/showGroups',tasksCtrl.showGroups);
router.post('/createGroupTask',tasksCtrl.upload.single('tasksAvatar'),tasksCtrl.createGroupTask);
router.post('/updateGroupTaskNewAvatar',tasksCtrl.upload.single('tasksAvatar'),tasksCtrl.updateGroupTaskNewAvatar);
router.post('/updateGroupTaskSameAvatar',tasksCtrl.updateGroupTaskSameAvatar);
router.post('/deleteTask',tasksCtrl.deleteTask);
router.post('/deleteGroup',tasksCtrl.deleteGroup);
router.post('/doneGroup',tasksCtrl.doneGroup);
router.post('/doneTask',tasksCtrl.doneTask);
router.post('/showNextPage',tasksCtrl.showCommonTasks);

module.exports = router;
