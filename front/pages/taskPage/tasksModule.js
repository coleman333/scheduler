const angular = require('angular');
const ngRoute = require('angular-route');
const ngMaterial = require('angular-material');
// const mainPageCtrl = require('./mainPageCtrl');

const ngFileUpload = require('ng-file-upload');
const tasksCtrl = require('./tasksCtrl');
const template = require('./tasks.html');

angular.module('tasks', [ngRoute, ngMaterial, ngFileUpload])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/tasks', {
            template: template,  //
            controller: 'tasksCtrl'
           
        });
    }])
    .controller('tasksCtrl', tasksCtrl);

module.exports = 'tasks';
