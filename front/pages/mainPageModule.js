const angular = require('angular');
const ngRoute = require('angular-route');
const ngMaterial = require('angular-material');
const mainPageCtrl = require('./mainPageCtrl');
const template = require('./mainPage.html');
// import templateDialog from './templateDialog.html';
// import SingInOrRegister from './singInOrRegister.html';
// import signInWindow from './signInWindow.html';
const ngFileUpload = require('ng-file-upload');
const moment = require('moment');

angular.module('mainPage', [ngRoute, ngMaterial, ngFileUpload])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/mainPage', {
            template: template,  //   
            controller: 'mainPageCtrl',
            resolve: {
                user: ['$http', '$q', '$route', '$rootScope', '$location',
                    function ($http, $q, $route, $rootScope, $location) {

                        var defer = $q.defer();
                        // if ($rootScope.user) {
                        //     return defer.resolve();
                        // }
                        $http.post('http://localhost:3000/api/users/isUser', {})
                            .then(res=> {
                                var user ={};
                                user.name= res.data.name;
                                user.avatar =  res.data.avatar;
                                $rootScope.user = user;
                                $location.path('/mainPage');
                                defer.resolve()
                            })
                            .catch(res=> {
                                $rootScope.user = undefined;
                                $location.path('/tasks');
                                defer.reject();
                            });
                        return defer.promise;
                    }],
                allTasks: ['$http', '$q', '$route', '$rootScope', '$location',
                    function ($http, $q, $route, $rootScope, $location) {
                        var defer = $q.defer();
                        const limit = 10;
                        const pageNumber = Number($location.search().page || 1);
                        const groupId = $location.search().group;
                        
                        $http.post('http://localhost:3000/api/tasks/showCommonTasks', {
                            limit: limit,
                            offset: ((pageNumber || 1) - 1) * limit,
                            ready_or_not: false,
                            groupId
                            // endDate: moment().endOf('day')
                            // 'pageNumber': pageNumber,
                        })
                            .then(res=> {
                                $rootScope.allTasks = res.data.tasks;
                                // var temp = Math.ceil(res.data.count / 10);
                                // $rootScope.pageLenth =[];
                                $rootScope.pageLenth = [...new Array(Math.ceil(res.data.count / limit)).keys()];
                                // for(var i=0;i<temp;i++){
                                //     $rootScope.pageLenth.push({'page':i+1,'role':''});
                                // }
                                // $rootScope.pageLenth[0].role ='active';
                                // $rootScope.pageLenth[5].role = 'hide';
                                // for(var j=0;j< $rootScope.pageLenth.length;j++){
                                //     if(j==5) {
                                //         $rootScope.pageLenth[i].role = 'hide';
                                //     }
                                // }
                                // var commontasks =res.data.tasks;
                                // var allTasks =[];
                                // var doneTasks =[];
                                // for(var i = 0; i< commontasks.length;i++){
                                //     if(commontasks[i].ready_or_not == true){
                                //         doneTasks.push(commontasks[i]);
                                //     }
                                //         allTasks.push(commontasks[i]);
                                // }
                                //  $rootScope.allTasks = allTasks;
                                // $rootScope.doneTasks = doneTasks;
                                defer.resolve(res.data);
                            })
                            .catch(res=> {
                                $rootScope.allTasks = [];
                                defer.reject();
                            });
                        return defer.promise;
                    }],
                Groups: ['$http', '$q', '$route', '$rootScope',
                    function ($http, $q, $route, $rootScope) {
                        var defer = $q.defer();
                        if ($rootScope.Groups) {
                            return defer.resolve();
                        }
                        $http.post('http://localhost:3000/api/tasks/showGroups', {})
                            .then(res=> {
                                var commonGroups = res.data;
                                var unreadyGroups = [];
                                var doneGroups = [];
                                // var doneTasks =[];
                                // var unreadyTasks = [];
                                for (var i = 0; i < commonGroups.length; i++) {
                                    if (commonGroups[i].ready_group_or_not == true) {
                                        doneGroups.push(commonGroups[i]);
                                        // for(var j=0;j<$rootScope.allTasks.length; j++){
                                        //     if($rootScope.allTasks[j].groupTasks == commonGroups[i]._id){
                                        //
                                        //     }
                                        // }
                                    }
                                    else {
                                        unreadyGroups.push(commonGroups[i]);
                                    }
                                }
                                $rootScope.doneGroups = doneGroups;
                                $rootScope.Groups = unreadyGroups;
                                // $rootScope.Groups = res.data;
                                // $rootScope.Groups = res.data;
                                defer.resolve()
                            })
                            .catch(res=> {
                                $rootScope.Groups = [];
                                defer.reject();
                            });
                        return defer.promise;
                    }]
            }
        });
    }])
    .controller('mainPageCtrl', mainPageCtrl);

module.exports = 'mainPage';