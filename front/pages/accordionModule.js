import angular from 'angular';
import ngRoute from 'angular-route';
import ngFileUpload from 'ng-file-upload';
import moment from 'moment';
import head from 'lodash/head';
import slice from 'lodash/slice';

angular.module('ui.bootstrap.demo', []).controller('AccordionDemoCtrl', ['$scope', '$rootScope', '$http', '$location',
    function ($scope, $rootScope, $http, $location) {
        $scope.oneAtATime = true;

        $scope.status = {
            isCustomHeaderOpen: false,
            isFirstOpen: true,
            isFirstDisabled: false
        };
        $scope.showCommonTasks = function () {
            $location.search({page: 1})
            // $http.post('http://localhost:3000/api/tasks/showCommonTasks/', {
            //     ready_or_not: false,
            //     // endDate: moment().endOf('isoweek')
            // })
            //     .then(function (res) {
            //             console.log(res.data.title);
            //             // $rootScope.TasksList =
            //             $scope.commonTasks = res.data;
            //             $scope.close();
            //         }
            //         , function (res) {
            //             alert("something go's wrong");
            //         });
        };

        $scope.goTo = (path)=> {
            $location.path(path).search({page: 1});
        };
        
        $scope.schedulePerWeek = function () { // 1...5   2 //if(2>=1 && 2<=5){}
            $location.path('/schedulePage').search({page: 1});
            // var scheduleWeek2 = [];
            // for (var i = 0; i < $rootScope.allTasks.length; i++) {
            //     if (moment().endOf('isoweek').diff($rootScope.allTasks[i].deadLine, "days") >= 0 &&
            //         moment().diff($rootScope.allTasks[i].deadLine, "days") <= 0) {
            //         scheduleWeek2.push($rootScope.allTasks[i]);
            //         $rootScope.schedulePerWeek = scheduleWeek2;
            //     }
            // console.log(222,moment($rootScope.allTasks[i].deadLine).format('YYYY-MM-DD HH:mm:SS'),moment().diff($rootScope.allTasks[i].deadLine, "minutes"))
            //    }
            //  console.log(1, $rootScope.schedulePerWeek);
            // console.log(234,moment().endOf('isoweek'))
        };
        $scope.schedulePerMonth = function () {
            $location.path('/schedulePerMonth').search({page: 1});
        };
        $scope.schedulePerYear = function () {
            $location.path('/schedulePerYear').search({page: 1});
        };
        $scope.schedulePerDay = function () {
            $location.path('/schedulePerDay').search({page: 1});
        };
        $scope.donePerWeek = function () {
            $location.path('/donePerWeek').search({page: 1});
        };
        $scope.donePerMonth = function () {
            $location.path('/donePerMonth').search({page: 1});
        };
        $scope.donePerYear = function () {
            $location.path('/donePerYear').search({page: 1});
        };
        $scope.donePerDay = function () {
            $location.path('/donePerDay').search({page: 1});
        };
        $scope.urgentTask = function () {
            $location.path('/urgentTask').search({page: 1});
        };
        $scope.uncertainTask = function () {
            $location.path('/uncertainTask').search({page: 1});
        };
    }])
    .controller('PopoverDemoCtrl', function ($scope, $sce) {
        $scope.dynamicPopover = {
            content: 'Hello, World!',
            templateUrl: 'myPopoverTemplate.html',
            title: 'Title'
        };

        $scope.placement = {
            options: [
                'top',
                'top-left',
                'top-right',
                'bottom',
                'bottom-left',
                'bottom-right',
                'left',
                'left-top',
                'left-bottom',
                'right',
                'right-top',
                'right-bottom'
            ],
            selected: 'top'
        };

        $scope.htmlPopover = $sce.trustAsHtml('<b style="color: red">I can</b> have <div class="label label-success">HTML</div> content');
    })
    .controller('sideNavNewTaskCtrl', ['$scope', '$timeout', '$mdSidenav', '$log', '$rootScope', '$http', '$location',
        function ($scope, $timeout, $mdSidenav, $log, $rootScope, $http, $location) {

            // $rootScope.doneTasks = [];
            // $rootScope.doneGroups = [];

            $scope.groupClick = function (id) {
                // console.log(3333333);
                $location.search({page: 1, group: id});
                // const limit = 10;
                // const pageNumber = Number($location.search().page || 1);
                // $http.post('http://localhost:3000/api/tasks/showCommonTasks', {
                //     limit: limit,
                //     offset: ((pageNumber || 1) - 1) * limit,
                //     ready_or_not: false,
                //     groupId:id
                //     // endDate: moment().endOf('day')
                //     // 'pageNumber': pageNumber,
                // })
                //     .then(res=> {
                //         $rootScope.allTasks = res.data.tasks;
                //         $rootScope.pageLenth = [...new Array(Math.ceil(res.data.count / limit)).keys()];
                //     })
                //     .catch(res=> {
                //         $rootScope.allTasks = [];
                //     });
            };


            $scope.toggleRight = buildToggler('right4');
            $scope.isOpenRight = function () {
                return $mdSidenav('right').isOpen();
            };

            $scope.allTasks = $rootScope.allTasks;
            $rootScope.$watchCollection('allTasks', function (newValue, oldValue) {
                // console.log(222, newValue)
                $scope.allTasks = newValue;
            });

            $scope.Groups = $rootScope.Groups;
            $rootScope.$watchCollection('Groups', function (newValue, oldValue) {
                $scope.Groups = newValue;
            });

            function debounce(func, wait, context) {
                var timer;
                return function debounced() {
                    var context = $scope, args = Array.prototype.slice.call(arguments);
                    $timeout.cancel(timer);
                    timer = $timeout(function () {
                        timer = undefined;
                        func.apply(context, args);
                    }, wait || 10);
                };
            }

            function buildDelayedToggler(navID) {
                return debounce(function () {
                    // Component lookup should always be available since we are not using `ng-if`
                    $mdSidenav(navID)
                        .toggle()
                        .then(function () {
                            $log.debug("toggle " + navID + " is done");
                        });
                }, 200);
            }

            function buildToggler(navID) {
                return function () {
                    // Component lookup should always be available since we are not using `ng-if`
                    $mdSidenav(navID)
                        .toggle()
                        .then(function () {
                            $log.debug("toggle " + navID + " is done");
                        });
                };
            }

            $scope.tasksList = [];

            $scope.addNewTask = function () {
                if (!($rootScope.allTasks[$rootScope.allTasks.length - 1]) ||
                    ($rootScope.allTasks[$rootScope.allTasks.length - 1].title != undefined))   //.title !=undefined)
                {
                    $scope.allTasks.push({});
                }
                else {
                    alert('first fill previous task');
                }
            };

            $scope.addNewGroup = function () {
                $scope.buildToggler('right5')();
            };

            $scope.subGroupArr = [];
            $scope.addSubGroup = function () {
                $scope.subGroupArr.push({});
            };

            $scope.deleteSubGroup = function (index) {
                $scope.subGroupArr.splice(index, 1);
            };

            $scope.uploadFiles = function (files) {
                $scope.tasksAvatar = head(files);
            };

            $scope.getAvatar = function (task) {
                var group = $rootScope.Groups.find(function (group) {
                    return group._id === task.groupTasks;
                });
                if (group) {
                    return group.tasksAvatar;
                }
            };

            $scope.deleteTask = function (id) {
                let idTask = {_id: id};

                $http.post('http://localhost:3000/api/tasks/deleteTask/', idTask)
                    .then(function (res) {
                            for (var i = 0; i < $rootScope.allTasks.length; i++) {
                                if ($rootScope.allTasks[i]._id == idTask._id) {

                                    $rootScope.allTasks.splice($rootScope.allTasks[i], 1);
                                    // console.log(111, task)
                                }
                            }
                        }
                        , function (res) {
                            alert("something go's wrong");
                        });
            };

            $scope.deleteGroup = function (id) {
                let idGroup = {_id: id};

                $http.post('http://localhost:3000/api/tasks/deleteGroup/', idGroup)
                    .then(function (res) {
                            for (var j = 0; j < $rootScope.allTasks.length; j++) {
                                if ($rootScope.allTasks[j].groupTasks == idGroup._id) {
                                    $rootScope.allTasks.splice(j, 1);
                                    // [2,7,8,9].indexOf(2) //
                                }
                            }
                            for (var i = 0; i < $rootScope.Groups.length; i++) {
                                if ($rootScope.Groups[i]._id == idGroup._id) {

                                    $rootScope.Groups.splice(i, 1);
                                    // console.log(111, task)
                                }
                            }
                        }
                        , function (res) {
                            alert("something go's wrong");
                        });
            };

            $scope.addSubGroup2 = function () {
                $scope.group.subGroupTasks.push({});
            };

            $scope.deleteSubGroup2 = function (index) {
                $scope.group.subGroupTasks.splice(index, 1);
            };

            let group3 = {};
            $scope.openEditGroupPanel = function (group2) {
                $scope.buildToggler('right6')();
                group3 = group2;
                $scope.group = group2;
                $scope.group.GroupDeadLine = new Date(group2.GroupDeadLine);

            };

            $scope.editGroup = function () {
                let group = {};

                if (!$scope.group.groupTitle) {
                    alert('to update the group at least must be entered title of the task');
                    return;
                }
                if ($scope.group.groupTitle != group3.groupTitle) {
                    group.groupTitle = $scope.group.groupTitle;
                }
                else {
                    group.groupTitle = group3.groupTitle
                }

                group._id = group3._id;

                //if all points are equal then return

                if ($scope.group.GroupDeadLine != group3.GroupDeadLine) {
                    group.GroupDeadLine = $scope.group.GroupDeadLine;
                }
                else {
                    group.GroupDeadLine = group3.GroupDeadLine
                }

                if ($scope.group.subGroupTasks.length && $scope.group.subGroupTasks != group3.subGroupTasks) {
                    group.subGroupTasks = $scope.group.subGroupTasks;
                }
                else {
                    group.subGroupTasks = group3.subGroupTasks
                }
                ;

                //make a possibility to delete the sub group
                if ($scope.tasksAvatar != undefined && $scope.tasksAvatar != group3.tasksAvatar) {
                    // groupTask.append('tasksAvatar', $scope.group.tasksAvatar);
                    console.log($scope.tasksAvatar + "    " + group3.tasksAvatar);
                    let groupTask = new FormData();
                    groupTask.append('tasksAvatar', $scope.tasksAvatar);
                    groupTask.append('group', JSON.stringify(group));
                    $http.post('http://localhost:3000/api/tasks/updateGroupTaskNewAvatar/', groupTask, { // req.body
                        headers: {'Content-Type': undefined}
                    })
                        .then(function (res) {
                                for (var i = 0; i < $rootScope.Groups; i++)
                                    if ($rootScope.Groups[i]._id == res.data._id) {
                                        $rootScope.Groups[i] = res.data;
                                        getAvatar();
                                        // $scope.tasksAvatar = res.data.tasksAvatar;
                                        //find why the avatar didn't express without f5
                                    }
                                $scope.close();
                            }
                            , function (res) {
                                alert("something go's wrong");
                            });
                }
                else {
                    console.log($scope.group.tasksAvatar + "    " + group3.tasksAvatar);
                    group.tasksAvatar = $scope.tasksAvatar;
                    $http.post('http://localhost:3000/api/tasks/updateGroupTaskSameAvatar/', group)
                        .then(function (res) {
                                for (var i = 0; i < $rootScope.Groups; i++)
                                    if ($rootScope.Groups[i]._id == res.data._id) {
                                        $rootScope.Groups[i] = res.data;
                                    }
                                $scope.close();
                            }
                            , function (res) {
                                alert("something go's wrong");
                            });
                }
            };
            let taskForEdit = {};
            $scope.openEditTaskPanel = function (item) {
                $scope.buildToggler('right7')();
                $scope.taskForEdit = item;
                taskForEdit = item;
                $scope.title = taskForEdit.title;
                $scope.deadLine = new Date(taskForEdit.deadLine);
                $scope.dateTask = new Date(taskForEdit.dateTask);
                $scope.periodHours = taskForEdit.periodHours;
                $scope.periodMinutes = taskForEdit.periodMinutes;
                $scope.groupTasks = taskForEdit.groupTasks;
                $scope.subGroupTasks = taskForEdit.subGroupTasks;

            };

            $scope.doneTask = function (id) {
                let idTask = {_id: id};

                $http.post('http://localhost:3000/api/tasks/doneTask/', idTask)
                    .then(function (res) {
                        for (var i = 0; i < $rootScope.allTasks.length; i++) {
                            if ($rootScope.allTasks[i]._id == idTask._id) {
                                $rootScope.allTasks.splice(i, 1);
                                // $scope.allTasks.splice($rootScope.allTasks[i], 1);
                                $scope.allTasks = $rootScope.allTasks;
                                // $rootScope.doneTasks.push($rootScope.allTasks[i]);
                            }
                        }
                    }, function (res) {
                        alert("something go's wrong");
                    });
            };

            $scope.doneGroup = function (id) {
                let idGroup = {_id: id};

                $http.post('http://localhost:3000/api/tasks/doneGroup/', idGroup)
                    .then(function (res) {
                        for (var j = 0; j < $rootScope.allTasks.length; j++) {
                            if ($rootScope.allTasks[j].groupTasks == idGroup._id) {
                                $rootScope.allTasks.splice(j, 1);
                                $rootScope.doneTasks.push($rootScope.allTasks[j]);
                            }
                        }
                        for (var i = 0; i < $rootScope.Groups.length; i++) {
                            if ($rootScope.Groups[i]._id == idGroup._id) {
                                $rootScope.Groups.splice(i, 1);
                                $rootScope.doneGroups.push($rootScope.Groups[i]);
                            }
                        }
                    }, function (res) {
                        alert("something go's wrong");
                    });
            };

            $scope.editTask = function () {
                let taskEdit = taskForEdit;
                if (!$scope.title) {
                    alert('to create the task at least must be entered title of the task');
                    return;
                }
                let task = {};
                if ($scope.title != taskEdit.title) {
                    task.title = $scope.title;
                }
                else {
                    task.title = taskEdit.title
                }
                task._id = taskForEdit._id;

                if ($scope.deadLine != taskEdit.deadLine) {
                    task.deadLine = $scope.deadLine;
                }
                else {
                    task.deadLine = taskEdit.deadLine
                }

                if ($scope.dateTask && $scope.dateTask != taskEdit.dateTask) {
                    task.dateTask = $scope.dateTask;
                }
                else {
                    task.dateTask = taskEdit.dateTask
                }

                if ($rootScope.periodHours && $rootScope.periodHours != taskEdit.periodHours) {
                    task.periodHours = $rootScope.periodHours;
                }
                else {
                    task.periodHours = taskEdit.periodHours
                }

                if ($rootScope.periodMinutes && $rootScope.periodMinutes != taskEdit.periodMinutes) {
                    task.periodMinutes = $rootScope.periodMinutes;
                }
                else {
                    task.periodMinutes = taskEdit.periodMinutes
                }

                if ($scope.groupTasks && $scope.groupTasks != taskEdit.groupTasks) {
                    task.groupTasks = $scope.groupTasks;
                }
                else {
                    task.groupTasks = taskEdit.groupTasks
                }

                if ($scope.subGroupTasks && $scope.subGroupTasks != taskEdit.subGroupTasks) {
                    task.subGroupTasks = $scope.subGroupTasks;
                }
                else {
                    task.subGroupTasks = taskEdit.subGroupTasks
                }


                $http.post('http://localhost:3000/api/tasks/updateTask/', task)
                    .then(function (res) {
                            for (var i = 0; i < $rootScope.allTasks.length; i++) {
                                if ($rootScope.allTasks[i]._id == task._id) {

                                    $rootScope.allTasks[i] = task;
                                    console.log(111, task)
                                }
                            }
                            $scope.close();
                        }
                        , function (res) {
                            alert("something go's wrong");
                        });
            };

            $scope.getSubGroupsById = function (mainGroupId) {
                return $rootScope.Groups.find(function (g) {
                    return g._id === mainGroupId;                   //if(g._id === mainGroupId){true}false
                });
            };

            $scope.createGroup = function () {
                if (!$scope.groupTitle) {
                    alert('to create the group at least must be entered title of the task');
                    return;
                }
                let groupTask = {
                    groupTitle: $scope.groupTitle
                };
                if ($scope.GroupDeadLine) {
                    groupTask.GroupDeadLine = $scope.GroupDeadLine;
                }
                if ($scope.subGroupArr.length) {
                    groupTask.subGroupTasks = $scope.subGroupArr;
                }
                let groupTask2 = new FormData();

                if ($scope.tasksAvatar) {
                    groupTask2.append('tasksAvatar', $scope.tasksAvatar);
                }
                groupTask2.append('groupTask', JSON.stringify(groupTask));

                $http.post('http://localhost:3000/api/tasks/createGroupTask/', groupTask2, { // req.body
                    headers: {
                        'Content-Type': undefined
                    }
                })
                    .then(function (res) {

                            $rootScope.Groups.push(res.data);
                            $scope.close();
                        }
                        , function (res) {
                            alert("something go's wrong");
                        });
            };

            $scope.createTask = function () {
                if (!$scope.title) {
                    alert('to create the task at least must be entered title of the task');
                    return;
                }
                let task = {
                    title: $scope.title
                };
                if ($scope.deadLine) {
                    task.deadLine = $scope.deadLine;
                }
                if ($scope.dateTask) {
                    task.dateTask = $scope.dateTask;
                }
                if ($rootScope.periodHours) {
                    task.periodHours = Number($rootScope.periodHours)
                }
                if ($rootScope.periodMinutes) {
                    task.periodMinutes = Number($rootScope.periodMinutes)
                }
                if ($scope.groupTasks) {
                    task.groupTasks = $scope.groupTasks;
                }
                if ($scope.subGroupTasks) {
                    task.subGroupTasks = $scope.subGroupTasks;
                }

                $http.post('http://localhost:3000/api/tasks/createTask/', task)
                    .then(function (res) {
                            // $scope.savedTask = res.data;
                            // for(var i=0;i<= $rootScope.allTasks.length)
                            if ($rootScope.allTasks.length <= 9) {
                                $rootScope.allTasks.push(res.data);
                            }

                            // $rootScope.allTasks = res.data.tasks;
                            // var temp = Math.ceil($rootScope.allTasks.length/ 10);
                            // $rootScope.pageLenth =[];

                            $scope.close();
                        }
                        , function (res) {
                            alert("something go's wrong");
                        });
            };

            $scope.buildToggler = buildToggler;

            $scope.close = function () {
                $mdSidenav('right4').close();
                $mdSidenav('right5').close();
                $mdSidenav('right6').close();
                $mdSidenav('right7').close()
                    .then(function () {
                        // $log.debug("close RIGHT is done");
                    });
            };

            $scope.openNavAddTask = function () {
                $scope.buildToggler('right4')();
                $scope.title = '';
                $scope.deadLine = '';
                $scope.dateTask = '';
                $scope.periodHours = 0;
                $scope.periodMinutes = 0;

            };

            $scope.convertDate = function (date) {
                // console.log(moment(date).date())
                if (!date) {
                    return;
                }
                return moment(date).format('YYYY-MM-DD')
            };

            $scope.slice = function (pagesArray) {
                let startIndex = $scope.activePage - 1 - 2;
                let endIndex = $scope.activePage + 2;

                if (startIndex < 0) {
                    endIndex -= startIndex;
                }
                if (endIndex > pagesArray.length) {
                    startIndex -= (endIndex - pagesArray.length);
                }

                if (startIndex < 0) {
                    startIndex = 0;
                }
                if (endIndex > pagesArray.length) {
                    endIndex = pagesArray.length;
                }
                // console.log(11,pagesArray, startIndex, endIndex, newPageArray)
                return slice(pagesArray, startIndex, endIndex);
            };

            $scope.activePage = Number($location.search().page || 1);
            $scope.limit = 10;
            $scope.nextPage = function (pageNumber) {
                if ($scope.activePage == pageNumber) {
                    return
                }
                // if ($location.search().group) {
                //     $location.search({page: group});
                // }
                // else {
                $location.search('page', pageNumber);
                // }

            };

            $scope.previous = function () {

            };

            $scope.next = function () {

            };

        }])
    .controller('TimepickerDemoCtrl', ['$scope', '$rootScope', '$http', '$log',
        function ($scope, $rootScope, $http, $log) {
            $scope.hstep = 1;
            $scope.mstep = 5;

            var d = new Date();
            d.setHours(0);
            d.setMinutes(0);
            $scope.mytime = d;

            $scope.ismeridian = false;
            $scope.toggleMode = function () {
                $scope.ismeridian = !$scope.ismeridian;
            };

            $scope.changed = function () {
                // $log.log('Time changed to: ' + $scope.mytime);
                $rootScope.periodHours = moment($scope.mytime).format("HH");
                $rootScope.periodMinutes = moment($scope.mytime).format("mm");
                // console.log('1111111111',$rootScope.periodHours ," : ",$rootScope.periodMinutes)
            };
        }]);
