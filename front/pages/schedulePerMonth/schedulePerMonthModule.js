const angular = require('angular');
const ngRoute = require('angular-route');
const ngMaterial = require('angular-material');
const schedulePerMonthCtrl = require('./schedulePerMonthCtrl');
const template = require('./schedulePerMonth.html');
const ngFileUpload = require('ng-file-upload');
const moment = require('moment');
// import moment from 'moment';

angular.module('schedulePerMonth', [ngRoute, ngMaterial,ngFileUpload])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/schedulePerMonth', {
            template: template,  //
            controller: 'schedulePerMonthCtrl',
           resolve:{
               schedulePerMonthArr:['$http', '$q', '$route','$rootScope','$location',
                   function ($http, $q, $route,$rootScope,$location) {
                       var defer = $q.defer();
                       const limit = 10;
                       const pageNumber = Number($location.search().page || 1);
                       $http.post('http://localhost:3000/api/tasks/showCommonTasks', {
                           ready_or_not:false,
                           limit: limit,
                           offset: ((pageNumber || 1) - 1) * limit,
                           endDate: moment().endOf('isoMonth')   })
                           .then(res=> {
                               $rootScope.schedulePerMonthArr =res.data.tasks;
                               $rootScope.pageLenth = [...new Array(Math.ceil(res.data.count / limit)).keys()];
                               defer.resolve(res.data);
                           })
                           .catch(res=> {
                               $rootScope.schedulePerMonthArr = [];
                               defer.reject();
                           });
                       return defer.promise;
                   }],
               Groups: ['$http', '$q', '$route', '$rootScope',
                   function ($http, $q, $route, $rootScope) {
                       var defer = $q.defer();
                       if ($rootScope.Groups) {
                           return defer.resolve();
                       }
                       $http.post('http://localhost:3000/api/tasks/showGroups', {})
                           .then(res=> {
                               var commonGroups = res.data;
                               var unreadyGroups = [];
                               var doneGroups = [];
                               // var doneTasks =[];
                               // var unreadyTasks = [];
                               for (var i = 0; i < commonGroups.length; i++) {
                                   if (commonGroups[i].ready_group_or_not == true) {
                                       doneGroups.push(commonGroups[i]);
                                       // for(var j=0;j<$rootScope.allTasks.length; j++){
                                       //     if($rootScope.allTasks[j].groupTasks == commonGroups[i]._id){
                                       //
                                       //     }
                                       // }
                                   }
                                   else {
                                       unreadyGroups.push(commonGroups[i]);
                                   }
                               }
                               $rootScope.doneGroups = doneGroups;
                               $rootScope.Groups = unreadyGroups;
                               // $rootScope.Groups = res.data;
                               // $rootScope.Groups = res.data;
                               defer.resolve()
                           })
                           .catch(res=> {
                               $rootScope.Groups = [];
                               defer.reject();
                           });
                       return defer.promise;
                   }]
           }
        });
    }])
    .controller('schedulePerMonthCtrl', schedulePerMonthCtrl);

module.exports = 'schedulePerMonth';
