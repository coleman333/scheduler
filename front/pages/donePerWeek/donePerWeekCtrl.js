import angular from 'angular';
import moment from 'moment';
const slice = require('lodash/slice');

module.exports= ['$scope','$rootScope','$http','$location', function ($scope,$rootScope,$http,$location) {

    $scope.slice = function (pagesArray) {
        let startIndex = $scope.activePage - 1 - 2;
        let endIndex = $scope.activePage + 2;

        if (startIndex < 0) {
            endIndex -= startIndex;
        }
        if (endIndex > pagesArray.length) {
            startIndex -= (endIndex - pagesArray.length);
        }

        if (startIndex < 0) {
            startIndex = 0;
        }
        if (endIndex > pagesArray.length) {
            endIndex = pagesArray.length;
        }
        // console.log(11,pagesArray, startIndex, endIndex, newPageArray)
        return slice(pagesArray, startIndex, endIndex);
    };

    $scope.activePage = Number($location.search().page || 1);
    $scope.limit = 10;
    $scope.nextPage = function (pageNumber) {
        if ($scope.activePage == pageNumber) {
            return
        }
        $location.search({page: pageNumber});
    }



    $scope.convertDate = function (date) {
        // console.log(moment(date).date())
        if (!date) {
            return;
        }
        return moment(date).format('YYYY-MM-DD')
    };

    $scope.getAvatar = function (task) {
        var group = $rootScope.Groups.find(function (group) {
            return group._id === task.groupTasks;
        });
        if (group) {
            return group.tasksAvatar;
        }
    };

}];
