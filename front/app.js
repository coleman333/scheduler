'use strict';
import angular from 'angular';
import ngRoute from 'angular-route';
import mainPage from './pages/mainPageModule';
import ngFileUpload from 'ng-file-upload';
import head from 'lodash/head';
const schedule = require('./pages/schedulePage/schedulePageModule');
const schedulePerMonth = require('./pages/schedulePerMonth/schedulePerMonthModule');
const schedulePerDay = require('./pages/schedulePerDay/schedulePerDayModule');
const schedulePerYear = require('./pages/schedulePerYear/schedulePerYearModule');

const donePerMonth = require('./pages/donePerMonth/donePerMonthModule');
const donePerWeek = require('./pages/donePerWeek/donePerWeekModule');
const donePerDay = require('./pages/donePerDay/donePerDayModule');
const donePerYear = require('./pages/donePerYear/donePerYearModule');

const urgentTask = require('./pages/urgentTasks/urgentTaskModule');
const certainTask = require('./pages/uncertainTask/uncertainTaskModule');

const tasks = require('./pages/taskPage/tasksModule');
import 'bootstrap/dist/js/bootstrap';
import './assets/js/ui-bootstrap-tpls-1.3.3.min';
const template = require('./index.html');
const accordionModule = require('./pages/accordionModule');

// angular.module('home',[])
//     .config(['$routeProvider', function ($routeProvider) {
//         $routeProvider.when('/home', {
//             template: '<body>HELLO!!!!</body>',  //
//             controller: 'mainPageCtrl'
//
//         });
//     }])
angular.module('myApp', [
    ngRoute,
    mainPage,
    ngFileUpload,
    schedule,
    schedulePerMonth,
    schedulePerDay,
    schedulePerYear,
    donePerMonth,
    donePerDay,
    donePerYear,
    donePerWeek,
    urgentTask,
    certainTask,
    tasks,
    'ui.bootstrap',
    'ui.bootstrap.demo'

]).config(['$locationProvider', '$routeProvider', '$mdIconProvider',
    function ($locationProvider, $routeProvider, $mdIconProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        // $locationProvider.hashPrefix('!');
        $routeProvider.otherwise({redirectTo: '/mainPage'});
    }])
// .run(['$location', '$rootScope', '$http', function ($location, $rootScope, $http) {
//     function isUser() {
//         $http.post('http://localhost:3000/api/users/isUser', {})
//             .then(res=> {
//                     $rootScope.user = res.data.user;
//                 },
//                 // .catch(res=> {
//                 //     $scope.user = res.data.user;
//                 // })
//                 function (res) {
//                     $rootScope.user = undefined;
//                     $location.path('/home');
//                 })
//     }
//
//     isUser();
// }])
    .controller('searchCtrl', ['$scope', '$http', '$location', '$rootScope',
        function ($scope, $http, $location, $rootScope) {
            $scope.searchTasks = function () {
                const limit = 10;
                const pageNumber = Number($location.search().page || 1);
                $http.post('http://localhost:3000/api/tasks/showCommonTasks', {
                    // limit: limit,
                    // offset: ((pageNumber || 1) - 1) * limit,
                    searchTasks: "searchTasks",
                    title: $scope.searchTask
                    // ready_or_not: false
                    // endDate: moment().endOf('day')
                    // 'pageNumber': pageNumber,
                })
                    .then(res=> {
                        $rootScope.allTasks = res.data.tasks;
                        $rootScope.pageLenth = [...new Array(Math.ceil(res.data.count / limit)).keys()];
                    })
                    .catch(res=> {
                        $rootScope.allTasks = [];
                    });
            };
        }])
    .controller('sideNavCommonCtrl', ['$scope', '$timeout', '$mdSidenav', '$log', '$rootScope', '$http', '$location',
        function ($scope, $timeout, $mdSidenav, $log, $rootScope, $http, $location) {
            $scope.toggleRight = buildToggler('right');
            $scope.isOpenRight = function () {
                return $mdSidenav('right').isOpen();
            };

            $rootScope.$watch('user', function (newValue, oldValue) {
                $scope.user = newValue;
                if (!$scope.user) {
                    $scope.name = 'enter';
                    $scope.isUser = false;
                }
                else {
                    $scope.isUser = true;
                    $scope.name = $scope.user.name
                }
            });


            function debounce(func, wait, context) {
                var timer;
                return function debounced() {
                    var context = $scope, args = Array.prototype.slice.call(arguments);
                    $timeout.cancel(timer);
                    timer = $timeout(function () {
                        timer = undefined;
                        func.apply(context, args);
                    }, wait || 10);
                };
            }

            function buildDelayedToggler(navID) {
                return debounce(function () {
                    // Component lookup should always be available since we are not using `ng-if`
                    $mdSidenav(navID)
                        .toggle()
                        .then(function () {
                            $log.debug("toggle " + navID + " is done");
                        });
                }, 200);
            }

            function buildToggler(navID) {
                return function () {
                    // Component lookup should always be available since we are not using `ng-if`
                    $mdSidenav(navID)
                        .toggle()
                        .then(function () {
                            $log.debug("toggle " + navID + " is done");
                        });
                };
            }

            $scope.logout = function () {
                $http.post('http://localhost:3000/api/users/logout', {})
                    .then(function (res) {
                        delete $rootScope.user;
                        $location.path('/home');
                    }, function (res) {

                    });
            };

            $scope.buildToggler = buildToggler;
        }])
    // .controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    //   $scope.close = function () {
    //     // Component lookup should always be available since we are not using `ng-if`
    //     $mdSidenav('left').close()
    //         .then(function () {
    //           $log.debug("close LEFT is done");
    //         });
    //
    //   };
    // })
    .controller('EnterCtrl', ['$scope', '$timeout', '$mdSidenav', '$log', '$http', '$location',
        function ($scope, $timeout, $mdSidenav, $log, $http, $location) {
            $scope.close = function () {
                $mdSidenav('right').close()
                    .then(function () {
                        $log.debug("close RIGHT is done");
                    });
            };
            $scope.openNavRegistration = function () {
                // toggleRight();
                // console.log('asdg');
                $scope.buildToggler('right')();
                $scope.buildToggler('right2')();
                // $scope.isOpenRight = function(){
                //     return $mdSidenav('right').isOpen();
                // };
            };
            $scope.openNavLogin = function () {
                // toggleRight();
                // console.log('sdfg');
                $scope.buildToggler('right')();
                $scope.buildToggler('right3')();
                // $scope.isOpenRight = function(){
                //     return $mdSidenav('right').isOpen();
                // };
            };

            $scope.searchTasks = function () {
                const limit = 10;
                const pageNumber = Number($location.search().page || 1);
                $http.post('http://localhost:3000/api/tasks/showCommonTasks', {
                    limit: limit,
                    offset: ((pageNumber || 1) - 1) * limit,
                    searchTasks: "searchTasks",
                    title: $scope.searchTask
                    // ready_or_not: false
                    // endDate: moment().endOf('day')
                    // 'pageNumber': pageNumber,
                })
                    .then(res=> {
                        $rootScope.allTasks = res.data.tasks;
                        $rootScope.pageLenth = [...new Array(Math.ceil(res.data.count / limit)).keys()];
                    })
                    .catch(res=> {
                        $rootScope.allTasks = [];
                    });
            };

        }])
    .controller('registrationSubmitCtrl', ['$scope', '$timeout', '$mdSidenav', '$log', '$http', '$location', '$rootScope',
        function ($scope, $timeout, $mdSidenav, $log, $http, $location, $rootScope) {

            $scope.close = function () {
                console.log('rewers');
                $mdSidenav('right2').close()
                    .then(function () {
                        // $log.debug("close RIGHT is done");
                    });
            };

            $scope.registration = function () {
                let user = {
                    name: $scope.name,
                    email: $scope.email,
                    password: $scope.password
                    //avatar:$scope.avatar
                };
                let data = new FormData();

                if ($scope.avatar) {
                    data.append('avatar', $scope.avatar);
                }
                data.append('user', JSON.stringify(user));

                $http.post('http://localhost:3000/api/users/registration/', data, {
                    headers: {
                        'Content-Type': undefined
                    }
                })
                    .then(function (res) {
                            console.log(res.data.name);
                            alert('now you chosen one NEO');
                            $scope.user = {};
                            $scope.user.avatar = res.data.avatar;                                        //?
                            $scope.close();
                            user.email = $scope.email;
                            user.password = $scope.password;
                            $http.post('http://localhost:3000/api/users/login', user)
                                .then(function (res) {
                                    alert('now you are the chosen one NEO');
                                    // $scope.close();
                                    $rootScope.user = res.data;
                                    $scope.isUser = true;
                                    $location.path('/mainPage');
                                }, function (res) {
                                    // homeCtrl.isIdentified = false;
                                    alert('one more miss and this computer will be deactivated');
                                });
                        }
                        , function (res) {
                            // homeCtrl.isIdentified = false;
                            alert('one more miss and this computer will be deactivated');
                        });
            };
            $scope.uploadFiles = function (files) {
                console.dir(files);
                $scope.avatar = head(files);
            };
        }])
    .controller('loginSubmitCtrl', ['$scope', '$timeout', '$mdSidenav', '$log', '$http', '$rootScope', '$location',
        function ($scope, $timeout, $mdSidenav, $log, $http, $rootScope, $location) {

            $scope.close = function () {
                $mdSidenav('right3').close()
                    .then(function () {
                        // $log.debug("close RIGHT is done");
                    });
            };

            $scope.login = function () {
                $scope.isUser = false;
                let user = {};
                user.email = $scope.email;
                user.password = $scope.password;
                $http.post('http://localhost:3000/api/users/login', user)
                    .then(function (res) {
                        alert('now you are the chosen one NEO');
                        $scope.close();
                        $rootScope.user = res.data;
                        $scope.isUser = true;
                        $location.path('/mainPage');
                    }, function (res) {
                        // homeCtrl.isIdentified = false;
                        alert('one more miss and this computer will be deactivated');
                    });
            };


        }])