FROM node:6.10.2

EXPOSE 3000

ADD . /scheduler

WORKDIR /scheduler

RUN npm i \
    && npm run build:dev

ENTRYPOINT ["npm", "run"]

CMD ["dev"]