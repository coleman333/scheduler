const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
const env = process.env.NODE_ENV;

let plugins = [
    new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.$": "jquery",
        "window.jQuery": "jquery"

    }),
    // new ExtractTextPlugin('styles/main.css'),
    new CopyWebpackPlugin([
        // {output}/file.txt
        {from: './index.html'},
        // {from: './css/app.css', to: './css/'},
        // {from: './css/buttonMaterial.css', to: './css/'},
        {from: './css/angular-material.min.css', to: './css/'},
        {from: './image/', to: './image/'},
        {from: './assets/dist/css/bootstrap.min.css', to: './css/'},
        {from: './assets/dist/css/bootstrap-theme.min.css', to: './css/'},
        {from: './assets/dist/fonts/', to: './fonts/'},
        {from: './css/css.css', to: './css/'},
        // {from: './css/inputMaterial.css', to:'./css/'},
        // {from: './css/registrationButton.css', to: './css/'}
    ], {})
];

if (env === 'production') {
    // Order the modules and chunks by occurrence.
    // This saves space, because often referenced modules
    // and chunks get smaller ids.
    plugins.push(new webpack.optimize.OccurrenceOrderPlugin());
    plugins.push(new webpack.optimize.UglifyJsPlugin({
        beautify: false,
        comments: false,
        compress: {
            dead_code: true,
            booleans: true,
            loops: true,
            unused: true,
            warnings: false,
            drop_console: true
        }
    }));
} else {
    plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = {
    devtool: env === 'production' ? 'source-map' : 'eval',
    context: path.join(__dirname, '/front'),
    entry: env === 'production' ? [
        'babel-polyfill',
        './app.js'
    ] : [
        'babel-polyfill',
        'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true',
        './app.js'
    ],
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'res_app.js',
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.scss'],
        modules: [
            'front', 'node_modules'
        ]
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: path.join(__dirname, 'front'),
                exclude: /node_modules/
            },
            {test: /\.json$/, loader: 'json-loader'},
            {test: /\.html$/, loader: 'html-loader'},
            {test: /\.(eot|woff|woff2|ttf|svg|png|gif)([\?]?.*)$/, loader: 'url-loader'},
            {test: /\.jpg$/, loader: 'file-loader'},
            // {
            //   test: /\.scss$/,
            //   loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass?includePaths[]='
            //     + encodeURIComponent(path.resolve(__dirname, 'app', 'assets', 'styles')))
            // }
        ]
    },
    plugins: plugins
};
